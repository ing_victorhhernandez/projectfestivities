package co.com.testivitiesSystem.etl.test;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.junit.Test;
import org.w3c.dom.NodeList;

import co.com.festivitiesSystem.Dtos.FestivitieDTO;
import co.com.festivitiesSystem.Exception.ExceptionETLFile;
import co.com.festivitiesSystem.etl.ExtractDataFestivities;
import co.com.festivitiesSystem.etl.TransformDataFestivities;

public class TransformDataFestivitiesTest {

	@Test
	public void testTransform() {
		ExtractDataFestivities extractDataFestivitiesTester;
		TransformDataFestivities transformDataFestivitiesTester = null;
		File fileFestivitiesXML;
		NodeList nodesFestivities;
		List<FestivitieDTO> festivitieDTOs;
		
		extractDataFestivitiesTester = new ExtractDataFestivities();
		fileFestivitiesXML = new File("/Users/victorHernandez/festivities.xml");
		extractDataFestivitiesTester.setFileFestivitiesXML(fileFestivitiesXML);
		transformDataFestivitiesTester= new TransformDataFestivities();
		try{
			nodesFestivities=extractDataFestivitiesTester.getNodesFestivitiesFromXMLFile();
			transformDataFestivitiesTester.setNodesFestivities(nodesFestivities);
			transformDataFestivitiesTester.TranformFestivitiesInListFestivitiesDTO();
			festivitieDTOs=transformDataFestivitiesTester.getFestivitiesToLoad();
			
			assertTrue(festivitieDTOs.size() == 1000);
		}catch(Exception e){
			assertTrue(false);
		}
		
	}

}
