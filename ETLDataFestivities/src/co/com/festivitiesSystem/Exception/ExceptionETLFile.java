package co.com.festivitiesSystem.Exception;

public class ExceptionETLFile extends Exception {

	public ExceptionETLFile(String message, Exception e) {
		super(message, e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
