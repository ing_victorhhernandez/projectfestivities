package co.com.festivitiesSystem.etl;


import java.sql.Time;
import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import co.com.festivitiesSystem.Dtos.FestivitieDTO;

public class TransformDataFestivities {
	
	private NodeList nodesFestivities;
	
	private List<FestivitieDTO> festivitiesToLoad;
	
	
	public void TranformFestivitiesInListFestivitiesDTO(){
		System.out.println("\nStart to import data from XML File :");
		FestivitieDTO festivitieDTO;
		festivitiesToLoad=new ArrayList<FestivitieDTO>();
		
		for (int indexFestiviti = 0; indexFestiviti < nodesFestivities.getLength(); indexFestiviti++) {
			festivitieDTO = new FestivitieDTO();
			Node festiviti = nodesFestivities.item(indexFestiviti);
					
			System.out.println("\nCurrent Element festiviti to import number :" +indexFestiviti+""+ festiviti.getNodeName());
					
			if (festiviti.getNodeType() == Node.ELEMENT_NODE) {

				Element FestivitiElement = (Element) festiviti;
				festivitieDTO.setName(FestivitiElement.getElementsByTagName("name").item(0).getTextContent());
				festivitieDTO.setPlace(FestivitiElement.getElementsByTagName("place").item(0).getTextContent());
				festivitieDTO.setStartDate(transformDateToTimestampWithZone(FestivitiElement.getElementsByTagName("start").item(0).getTextContent()));
				festivitieDTO.setEndDate(transformDateToTimestampWithZone(FestivitiElement.getElementsByTagName("end").item(0).getTextContent()));
				festivitieDTO.setZone("GMT0");
				festivitiesToLoad.add(festivitieDTO);
			}
		}
	  }


	private Timestamp transformDateToTimestampWithZone(String startDate) {
		Calendar calendarFestivities;
		Timestamp dateTimeStamp;
		calendarFestivities = DatatypeConverter.parseDateTime(startDate);
		dateTimeStamp = new Timestamp(calendarFestivities.getTimeInMillis());
		return dateTimeStamp;
	}


	public NodeList getNodesFestivities() {
		return nodesFestivities;
	}


	public void setNodesFestivities(NodeList nodesFestivities) {
		this.nodesFestivities = nodesFestivities;
	}


	public List<FestivitieDTO> getFestivitiesToLoad() {
		return festivitiesToLoad;
	}


	public void setFestivitiesToLoad(List<FestivitieDTO> festivitiesToLoad) {
		this.festivitiesToLoad = festivitiesToLoad;
	}
	    
}


