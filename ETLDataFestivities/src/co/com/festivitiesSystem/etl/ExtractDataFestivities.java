package co.com.festivitiesSystem.etl;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import co.com.festivitiesSystem.Exception.ExceptionETLFile;

public class ExtractDataFestivities {
	
	private final static String NAME_OF_NODES = "festivity"; 
	
	
	private File fileFestivitiesXML;
	
	
	public NodeList getNodesFestivitiesFromXMLFile() throws ExceptionETLFile{
		DocumentBuilderFactory dbFactory;
		DocumentBuilder dBuilder;
		Document document;
		NodeList nodesFestivities;
		try{
			dbFactory = DocumentBuilderFactory.newInstance();
			dBuilder = dbFactory.newDocumentBuilder();
			document= dBuilder.parse(fileFestivitiesXML);
			document.getDocumentElement().normalize();
		}catch(Exception e){
			throw new ExceptionETLFile("the system can't read the file.",e);
		}
		nodesFestivities = document.getElementsByTagName(NAME_OF_NODES);
		return nodesFestivities;
	}

	public File getFileFestivitiesXML() {
		return fileFestivitiesXML;
	}

	public void setFileFestivitiesXML(File fileFestivitiesXML) {
		this.fileFestivitiesXML = fileFestivitiesXML;
	}
	
	

}
