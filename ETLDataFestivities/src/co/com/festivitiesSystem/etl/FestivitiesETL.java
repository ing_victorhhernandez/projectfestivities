package co.com.festivitiesSystem.etl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.NodeList;

import co.com.festivitiesSystem.Dtos.FestivitieDTO;
import co.com.festivitiesSystem.Exception.ExceptionETLFile;

public class FestivitiesETL {
	
	private ExtractDataFestivities extractDataFestivities;
	
	private TransformDataFestivities transformDataFestivities;
	
	private LoadDataFestivities loadDataFestivities;

	
	
	public void etlFestivitiesProcess(){
		File fileFestivitiesXML;
		NodeList nodesFestivities;
		List<FestivitieDTO>	festivitieDTOs= new ArrayList<FestivitieDTO>();
		
		fileFestivitiesXML = new File("/Users/victorHernandez/festivities.xml");
		extractDataFestivities = new ExtractDataFestivities();
		extractDataFestivities.setFileFestivitiesXML(fileFestivitiesXML);
		try {
			nodesFestivities=extractDataFestivities.getNodesFestivitiesFromXMLFile();
			transformDataFestivities.setNodesFestivities(nodesFestivities);
			transformDataFestivities.TranformFestivitiesInListFestivitiesDTO();
			festivitieDTOs=transformDataFestivities.getFestivitiesToLoad();
		} catch (ExceptionETLFile e) {
			e.printStackTrace();
		}
		
	}
	
	public ExtractDataFestivities getExtractDataFestivities() {
		return extractDataFestivities;
	}

	public void setExtractDataFestivities(ExtractDataFestivities extractDataFestivities) {
		this.extractDataFestivities = extractDataFestivities;
	}

	public TransformDataFestivities getTransformDataFestivities() {
		return transformDataFestivities;
	}

	public void setTransformDataFestivities(TransformDataFestivities transformDataFestivities) {
		this.transformDataFestivities = transformDataFestivities;
	}

	public LoadDataFestivities getLoadDataFestivities() {
		return loadDataFestivities;
	}

	public void setLoadDataFestivities(LoadDataFestivities loadDataFestivities) {
		this.loadDataFestivities = loadDataFestivities;
	}
	


}
