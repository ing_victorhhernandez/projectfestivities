package co.com.festivitiesSystem.etl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.GenericArrayType;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import co.com.festivitiesSystem.Dtos.FestivitieDTO;

public class LoadDataFestivities {
	
	private static final String DRIVER="org.postgresql.Driver";
	private static final String URL="jdbc:postgresql://localhost:5432/postgres";
	private static final String USER="postgres";
	private static final String PASSWORD="Admin.123";
	
	
	private static final String QUERY_INSERT_RECORD_TO_THE_TABLE_FESTIVITI="INSERT INTO \"FestivitiesProject\" .\"FESTIVITY\" VALUES(?,?,?,?,?,?) ";

	
	private List<FestivitieDTO> festivitieDTOs;
	
	
	
	
	public void loadDataFestivitiesInsertDataBase() throws ClassNotFoundException, IOException, SQLException{
		Connection connection = null;
		
		connection = getConnection();
		insertFestivitiesIntoTheTables(connection);
	}

	
	

	private void insertFestivitiesIntoTheTables(Connection connection) throws SQLException {
		PreparedStatement preparedStatement = null;		
		int rowNumber=0;
		
		String insertTableSQL = QUERY_INSERT_RECORD_TO_THE_TABLE_FESTIVITI;
		
		preparedStatement=connection.prepareStatement(insertTableSQL);
		
		for(FestivitieDTO festivitieDTO: festivitieDTOs){
			rowNumber++;
			insertValuesIntoQuery(preparedStatement,festivitieDTO,rowNumber);
			System.out.println("inserto the festivity: "+festivitieDTO.getName());
		}
		
	}




	private void insertValuesIntoQuery(PreparedStatement preparedStatement, FestivitieDTO festivitieDTOToInsert, int rowNumber) throws SQLException {
		preparedStatement.setString(1,festivitieDTOToInsert.getName());
		preparedStatement.setString(2,festivitieDTOToInsert.getPlace());
		preparedStatement.setTimestamp(3,festivitieDTOToInsert.getStartDate());
		preparedStatement.setTimestamp(4,festivitieDTOToInsert.getEndDate());
		preparedStatement.setString(5, "GTM0");
		preparedStatement.setInt(6, rowNumber);
		preparedStatement.executeUpdate();
		System.out.println("Record is inserted into Festivity table!");
	}




	private Connection getConnection() throws IOException, ClassNotFoundException, SQLException {
		Connection connection = null;
		
		
		Class.forName(DRIVER);
		
		connection = DriverManager.getConnection(URL,USER,PASSWORD);
		
		return connection;
	}









	public List<FestivitieDTO> getFestivitieDTOs() {
		return festivitieDTOs;
	}

	public void setFestivitieDTOs(List<FestivitieDTO> festivitieDTOs) {
		this.festivitieDTOs = festivitieDTOs;
	}
	
	

}
