package com.festivities.services;

import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.festivities.business.festivitiesBeanService;
import com.festivities.dtos.FestivityDto;

@RequestScoped
@Path("")
@Produces({ "application/xml", "application/json" })
@Consumes({ "application/xml", "application/json" })
public class ServicesFestivities {
	
	@EJB
	festivitiesBeanService festivitiesBeanService;
	
	@GET
	@Path("/allFestivities/")
	@Produces(MediaType.APPLICATION_JSON)
	public List<FestivityDto> getAllFestivities() {

	   return festivitiesBeanService.getAllExistentFestivities();

	}

}
