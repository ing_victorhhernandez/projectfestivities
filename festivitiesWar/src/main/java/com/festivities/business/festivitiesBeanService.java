package com.festivities.business;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import com.festivities.dtos.FestivityDto;
import com.festivities.entities.Festivity;

/**
 * Session Bean implementation class festivitiesBeanServices
 */
@Stateless
@LocalBean
public class festivitiesBeanService {
	
	@PersistenceContext(unitName="FestivitiesProject-ejb")
	EntityManager em;

    /**
     * Default constructor. 
     */
    public festivitiesBeanService() {
        // TODO Auto-generated constructor stub
    }
    
    
    public FestivityDto festivityByName(String name){
    	Query query;
    	Festivity festivity;
    	FestivityDto festivityDto;
    	
    	query = em.createQuery("Select festivity from Festivity festivity where festivity.name :=name");
    	
    	query.setParameter("name", "name");
    	festivity=(Festivity) query.getSingleResult();
    	festivityDto= transformFestivityToFestivityDto(festivity); 
    	
    	return festivityDto;
    }
    
  
    
    private FestivityDto transformFestivityToFestivityDto(Festivity festivity) {
		
    	FestivityDto festivityDto;
    	
    	festivityDto = new FestivityDto();
		festivityDto.setName(festivity.getName());
		festivityDto.setPlace(festivity.getPlace());
		festivityDto.setZone(festivity.getZone());
		festivityDto.setStartDate(tranformZoneTimeToLocalTime(festivity.getStarDate(),festivity.getZone()));
		festivityDto.setEndDate(tranformZoneTimeToLocalTime(festivity.getEndDate(),festivity.getZone()));
		
		return festivityDto;
	}


	public List<FestivityDto> getAllExistentFestivities(){
    	
    	Query query;
    	List<Festivity> festivities;
    	List<FestivityDto> festivityDtos;
    	
    	query = em.createQuery("Select festivity from Festivity festivity");
    	festivities=query.getResultList();
    	festivityDtos= transformFestivityToFestivityDto(festivities); 
    	
    	return festivityDtos;
    }


	private List<FestivityDto> transformFestivityToFestivityDto(List<Festivity> festivities) {
		
		List<FestivityDto> festivityDtos;
		FestivityDto festivityDto;
		
		festivityDtos = new ArrayList<FestivityDto>();
		
		for (Festivity festivity : festivities) {
			festivityDto = new FestivityDto();
			festivityDto.setName(festivity.getName());
			festivityDto.setPlace(festivity.getPlace());
			festivityDto.setZone(festivity.getZone());
			festivityDto.setStartDate(tranformZoneTimeToLocalTime(festivity.getStarDate(),festivity.getZone()));
			festivityDto.setEndDate(tranformZoneTimeToLocalTime(festivity.getEndDate(),festivity.getZone()));
			festivityDtos.add(festivityDto);
		}
		return festivityDtos;
	}


	private Timestamp tranformZoneTimeToLocalTime(Timestamp time, String timeZone) {
		
		    Calendar calendar = Calendar.getInstance();
		    calendar.setTimeInMillis(time.getTime());
		    
	        TimeZone fromTimeZone = TimeZone.getTimeZone(timeZone);
	        TimeZone toTimeZone = TimeZone.getDefault();
	        
	        Timestamp timeLocalTime;

	        calendar.setTimeZone(fromTimeZone);
	        calendar.add(Calendar.MILLISECOND, fromTimeZone.getRawOffset() * -1);
	        if (fromTimeZone.inDaylightTime(calendar.getTime())) {
	            calendar.add(Calendar.MILLISECOND, calendar.getTimeZone().getDSTSavings() * -1);
	        }

	        calendar.add(Calendar.MILLISECOND, toTimeZone.getRawOffset());
	        if (toTimeZone.inDaylightTime(calendar.getTime())) {
	            calendar.add(Calendar.MILLISECOND, toTimeZone.getDSTSavings());
	        }
	        
	        timeLocalTime = new Timestamp(calendar.getTimeInMillis());
	       
		return timeLocalTime;
	}

}
