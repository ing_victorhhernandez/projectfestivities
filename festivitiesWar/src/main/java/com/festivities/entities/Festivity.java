package com.festivities.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;


/**
 * The persistent class for the "FESTIVITY" database table.
 * 
 */
@Entity
@Table(name="\"FESTIVITY\"" , schema="\"FestivitiesProject\"")
@NamedQuery(name="Festivity.findAll", query="SELECT f FROM Festivity f")
public class Festivity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="FESTIVITY_ID_GENERATOR", sequenceName="SEQUENCE_ID_FESTIVITY")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FESTIVITY_ID_GENERATOR")
	@Column(name="\"ID\"")
	private long id;

	@Column(name="\"END_DATE\"")
	private Timestamp endDate;

	@Column(name="\"NAME\"")
	private String name;

	@Column(name="\"PLACE\"")
	private String place;

	@Column(name="\"STAR_DATE\"")
	private Timestamp starDate;

	@Column(name="\"ZONE\"")
	private String zone;

	public Festivity() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Timestamp getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPlace() {
		return this.place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Timestamp getStarDate() {
		return this.starDate;
	}

	public void setStarDate(Timestamp starDate) {
		this.starDate = starDate;
	}

	public String getZone() {
		return this.zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

}